<div data-ng-controller="NewsController" data-ng-init="getNews('/panel/news/edit/')">
        <form>
                <div class="form-group">
                        <label>Заголовок:</label>
                        <input type="text" class="form-control" data-ng-model="news.title">
                </div>
                <div class="form-group">
                        <label>Краткое писание:</label>
                        <input type="text" class="form-control" data-ng-model="news.description">
                </div>
                <div class="form-group">
                        <label>Ссылка на изображение:</label>
                        <input type="text" class="form-control" data-ng-model="news.image">
                </div>
                <div class="form-group">
                        <label>Текст новости:</label>
                        <textarea class="form-control" data-ng-model="news.text"></textarea>
                </div>
                <div class="form-group">
                        <label>Опубликовать новость:</label>
                        <input type="checkbox" data-ng-model="news.isActive">
                </div>
                <div class="form-group">
                        <input type="button" class="btn btn-primary" data-ng-click="editNews()" value="Редактировать новость">
                </div>
        </form>
        <p data-ng-cloak class="alert alert-success" data-ng-show="editNewsSuccess">Новость успешно отредактирована</p>
        <p data-ng-cloak class="alert alert-danger" data-ng-show="editNewsFail">При редактировании новости произошла ошибка. Попробуйте повторить позже.</p>
</div>