<div data-ng-controller="CategoryController" data-ng-init="getCategory('/panel/category/edit/')">
    <form>
        <div class="form-group">
            <label>Заголовок:</label>
            <input type="text" class="form-control" data-ng-model="category.title">
        </div>
        <div class="form-group">
            <label>Краткое писание:</label>
            <input type="text" class="form-control" data-ng-model="category.description">
        </div>
        <div class="form-group">
            <input type="button" class="btn btn-primary" data-ng-click="editCategory()" value="Редактировать категорию">
        </div>
    </form>
    <p data-ng-cloak class="alert alert-success" data-ng-show="editCategorySuccess">Категория успешно отредактирована</p>
    <p data-ng-cloak class="alert alert-danger" data-ng-show="editCategoryFail">При редактировании категории произошла ошибка. Попробуйте повторить позже.</p>
</div>