<?php
$this->title = 'Панель редактирования';
?>
<div class="site-index" data-ng-controller="SiteController">
    <div class="content">
        <div class="row">
            <section data-ng-cloak class="col-xs-8" data-ng-init="getNewsList(false)">
                <h2>Список новостей</h2>
                <div data-ng-repeat="news in newsList | filter:filterByCategory">
                    <h3>{{news.title}}</h3>
                    <p>{{news.description}}</p>
                    <p>{{news.date}}</p>
                    <p><a href="/panel/news/edit/{{news.id}}">Редактировать новость >></a></p>
                </div>
            </section>
            <div data-ng-cloak class="col-xs-4" data-ng-init="getCategoryList()">
                <h2>Список категорий</h2>
                <ul>
                    <li data-ng-repeat="category in categoryList">
                        <b>{{category.title}}</b><br>
                        <a href="/panel/category/edit/{{category.id}}">Редактировать категорию</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>