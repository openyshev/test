<?php

namespace app\modules\panel\controllers;

use Yii;
use yii\web\Controller;

class IndexController extends Controller
{
    public $defaultAction = 'index';
    public function actionIndex()
    {
        return $this->render('/index');
    }
}