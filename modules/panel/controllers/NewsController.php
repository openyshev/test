<?php

namespace app\modules\panel\controllers;

use Yii;
use yii\web\Controller;

class NewsController extends Controller
{
    public function actionEdit()
    {
        if (!isset($_GET['id'])) {
            return $this->redirect('/panel');
        }
        return $this->render('/news/edit');
    }
}