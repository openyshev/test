<?php

namespace app\modules\api\components;
use yii\web\Controller;

class ApiController extends Controller
{
    public $enableCsrfValidation = false;

    protected function sendJsonResponse($statusCode, $body = null, $async = false)
    {
        $statusCodeMessages = [
            200 => 'OK',
            201 => 'Created',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            403 => 'Forbidden',
            404 => 'Not Found',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
        ];
        if (!isset($statusCodeMessages[$statusCode])) {
            throw new CException(sprintf('The status code %d does not exist.', $statusCode));
        }
        header($_SERVER['SERVER_PROTOCOL'] . ' ' . $statusCode . ' ' . $statusCodeMessages[$statusCode]);
        header('Content-type: application/json; charset=utf-8');
        echo json_encode($body, JSON_UNESCAPED_UNICODE);

        session_write_close();
        fastcgi_finish_request();
        //\Yii::app()->end();
    }
    protected function sendInternalError($e, array $response)
    {
        if (is_array($e)) {
            \Yii::log($e['message'], 'error');
        } elseif ($e instanceof Exception) {
            \Yii::log($e->getMessage(), 'error');
        }
        $response['meta']['code'] = 500;
        $this->sendJsonResponse($response['meta']['code'], $response);
    }
    public function sendError($code, array $response)
    {
        $response['meta']['code'] = $code;
        $this->sendJsonResponse($response['meta']['code'], $response);
    }
}