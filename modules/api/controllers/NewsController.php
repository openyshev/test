<?php

namespace app\modules\api\controllers;

use app\modules\api\components\ApiController;
use app\models\News;

class NewsController extends ApiController
{
    public function actionIndex()
    {
        $metaCode = 200;
        $requestParams = json_decode(file_get_contents('php://input'), true);

        $whereCriteria = [];
        if (isset($requestParams['activeFilter']) && $requestParams['activeFilter'] == 'true') {
            $whereCriteria['isActive'] = 1;
        }

        if (!$result = News::find()->where($whereCriteria)->with('categories')->asArray()->all()) {
            $metaCode = 404;
        }

        $this->sendJsonResponse($metaCode,
            [
                "meta" => $metaCode,
                "result" => $result
            ]
        );
    }

    public function actionGet()
    {
        $metaCode = 200;
        $result = [];
        $requestParams = json_decode(file_get_contents('php://input'), true);

        if (isset($requestParams['newsId'])) {
            $newsId = (int)$requestParams['newsId'];
            $whereCriteria = ['id' => $newsId];

            if (isset($requestParams['activeFilter']) && $requestParams['activeFilter'] == 'true') {
                $whereCriteria['isActive'] = 1;
            }

            if (!$result = News::find()->where($whereCriteria)->asArray()->all()[0]) {
                $metaCode = 404;
            }
        } else {
            $metaCode = 400;
        }


        $this->sendJsonResponse($metaCode,
            [
                "meta" => $metaCode,
                "result" => $result
            ]
        );
    }

    public function actionEdit()
    {
        $metaCode = 200;
        $result = [];
        $requestParams = json_decode(file_get_contents('php://input'), true);

        if (isset($requestParams['news'])) {
            $newsData = $requestParams['news'];
            $newsData['isActive'] = (int)$newsData['isActive'];

            if ($news = News::findOne((int)$newsData['id'])) {
                $news->setAttributes($newsData);
                if (!$news->save()) {
                    $metaCode = 400;
                }
            } else {
                $metaCode = 400;
            }
        } else {
            $metaCode = 400;
        }


        $this->sendJsonResponse($metaCode,
            [
                "meta" => $metaCode,
                "result" => $result
            ]
        );
    }
}