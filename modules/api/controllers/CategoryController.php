<?php

namespace app\modules\api\controllers;

use app\models\Category;
use app\modules\api\components\ApiController;

class CategoryController extends ApiController
{
    public function actionIndex()
    {
        $metaCode = 200;

        if (!$result = Category::find()->where(['isActive' => 1])->asArray()->all()) {
            $metaCode = 404;
        }

        $this->sendJsonResponse($metaCode,
            [
                "meta" => $metaCode,
                "result" => $result
            ]
        );
    }

    public function actionGet()
    {
        $metaCode = 200;
        $result = [];
        $requestParams = json_decode(file_get_contents('php://input'), true);

        if (isset($requestParams['categoryId'])) {
            $categoryId = (int)$requestParams['categoryId'];
            $whereCriteria = ['id' => $categoryId];

            if (isset($requestParams['activeFilter']) && $requestParams['activeFilter'] == 'true') {
                $whereCriteria['isActive'] = 1;
            }

            if (!$result = Category::find()->where($whereCriteria)->asArray()->all()[0]) {
                $metaCode = 404;
            }
        } else {
            $metaCode = 400;
        }


        $this->sendJsonResponse($metaCode,
            [
                "meta" => $metaCode,
                "result" => $result
            ]
        );
    }

    public function actionEdit()
    {
        $metaCode = 200;
        $result = [];
        $requestParams = json_decode(file_get_contents('php://input'), true);

        if (isset($requestParams['category'])) {
            $categoryData = $requestParams['category'];
            $categoryData['isActive'] = (int)$categoryData['isActive'];

            if ($news = Category::findOne((int)$categoryData['id'])) {
                $news->setAttributes($categoryData);
                if (!$news->save()) {
                    $metaCode = 400;
                }
            } else {
                $metaCode = 400;
            }
        } else {
            $metaCode = 400;
        }


        $this->sendJsonResponse($metaCode,
            [
                "meta" => $metaCode,
                "result" => $result
            ]
        );
    }
}