'use strict';
var app = angular.module('app', []);

app.filter('nl2br', ['$sce', function ($sce) {
    return function (text) {
        return text ? $sce.trustAsHtml(text.replace(/\n/g, '<br/>')) : '';
    };
}]);

app.controller('SiteController', ['$scope', '$http', '$window', '$location', function ($scope, $http, $window, $location) {
    $scope.newsList = [];

    $scope.getNewsList = function (activeFilter) {
        $http.post('/api/news', {activeFilter : activeFilter})
            .success(function (data) {
                $scope.newsList = data.result;
            })
            .error(function (data) {
            });
    };

    $scope.categoryList = [];
    $scope.getCategoryList = function () {
        $http.post('/api/category')
            .success(function (data) {
                $scope.categoryList = data.result;
            })
            .error(function (data) {
            });
    };

    $scope.selectedCategoryId = null;

    $scope.changeSelectedCategory = function (categoryId) {
        $scope.selectedCategoryId = categoryId;
    };

    $scope.filterByCategory = function (news) {
        if ($scope.selectedCategoryId) {
            for (var i = 0; i < news.categories.length; i++) {
                if (news.categories[i].id == $scope.selectedCategoryId) return true;
            }
        } else {
            return true;
        }

        return false;
    };
}]);

app.controller('NewsController', ['$scope', '$http', '$window', '$location', function ($scope, $http, $window, $location) {
    $scope.news = null;

    $scope.getNews = function(location, activeFilter){
        var newsId = parseInt($window.location.pathname.replace(location, ''));
            activeFilter = activeFilter || false;

        $http.post('/api/news/get', {newsId : newsId, activeFilter : activeFilter})
            .success(function (data) {
                $scope.news = data.result;
                $scope.news.isActive = $scope.news.isActive == "1";
            })
            .error(function (data) {
                $window.location.href = '/';
            });
    };

    $scope.editNewsSuccess = false;
    $scope.editNewsFail = false;
    $scope.editNews = function(){
        $scope.editNewsFail = false;
        $scope.editNewsSuccess = false;
        $http.post('/api/news/edit', {news : $scope.news})
            .success(function (data) {
                $scope.editNewsSuccess = true;
                console.log('edit successful!');
            })
            .error(function (data) {
                $scope.editNewsFail = true;
                $window.location.href = '/';
            });
    };
}]);

app.controller('CategoryController', ['$scope', '$http', '$window', '$location', function ($scope, $http, $window, $location) {
    $scope.category = null;

    $scope.getCategory = function(location, activeFilter){
        var categoryId = parseInt($window.location.pathname.replace(location, ''));
        activeFilter = activeFilter || false;

        $http.post('/api/category/get', {categoryId : categoryId, activeFilter : activeFilter})
            .success(function (data) {
                $scope.category = data.result;
                $scope.category.isActive = $scope.category.isActive == "1";
            })
            .error(function (data) {
                $window.location.href = location;
            });
    };

    $scope.editCategorySuccess = false;
    $scope.editCategoryFail = false;
    $scope.editCategory = function(){
        $scope.editCategoryFail = false;
        $scope.editCategorySuccess = false;
        $http.post('/api/category/edit', {category : $scope.category})
            .success(function (data) {
                $scope.editCategorySuccess = true;
                console.log('edit successful!');
            })
            .error(function (data) {
                $scope.editCategoryFail = true;
                $window.location.href = location;
            });
    };
}]);

