<?php

use yii\db\Schema;
use yii\db\Migration;

class m150516_064800_newsCategoryForeignKeys extends Migration
{
    public function up()
    {
        $this->addForeignKey('ForeignKeyNewsId', 'newsCategory', 'newsId', 'news', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('ForeignKeyCategoryId', 'newsCategory', 'categoryId', 'category', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('ForeignKeyNewsId', 'newsCategory');
        $this->dropForeignKey('ForeignKeyCategoryId', 'newsCategory');
    }
}
