<?php

use yii\db\Schema;
use yii\db\Migration;

class m150516_051315_categoryTableCreate extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        $this->createTable('{{%category}}', [
            'id' => Schema::TYPE_PK,
            'parentCategoryId' => Schema::TYPE_INTEGER . ' DEFAULT NULL',
            'description' => Schema::TYPE_TEXT . ' NOT NULL',
            'title' => Schema::TYPE_STRING . ' NOT NULL',
            'isActive' => Schema::TYPE_BOOLEAN . ' DEFAULT 0'
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%category}}');
    }
}
