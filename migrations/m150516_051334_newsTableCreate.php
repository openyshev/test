<?php

use yii\db\Schema;
use yii\db\Migration;

class m150516_051334_newsTableCreate extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        $this->createTable('{{%news}}', [
            'id' => Schema::TYPE_PK,
            'text' => Schema::TYPE_TEXT . ' NOT NULL',
            'description' => Schema::TYPE_TEXT . ' NOT NULL',
            'date' => Schema::TYPE_DATE . ' NOT NULL',
            'title' => Schema::TYPE_STRING . ' NOT NULL',
            'image' => Schema::TYPE_STRING . ' NOT NULL',
            'isActive' => Schema::TYPE_BOOLEAN . ' DEFAULT 0'
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%news}}');
    }
}
