<?php

use yii\db\Schema;
use yii\db\Migration;

class m150516_051307_newsCategoryTableCreate extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        $this->createTable('{{%newsCategory}}', [
            'id' => Schema::TYPE_PK,
            'newsId' => Schema::TYPE_INTEGER . ' NOT NULL',
            'categoryId' => Schema::TYPE_INTEGER . ' NOT NULL'
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%newsCategory}}');
    }
}
