<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "newsCategory".
 *
 * @property integer $id
 * @property integer $newsId
 * @property integer $categoryId
 */
class NewsCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'newsCategory';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['newsId', 'categoryId'], 'required'],
            [['newsId', 'categoryId'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'newsId' => 'News ID',
            'categoryId' => 'Category ID',
        ];
    }
}
