<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property integer $parentCategoryId
 * @property string $description
 * @property string $title
 * @property integer $isActive
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parentCategoryId', 'isActive'], 'integer'],
            [['description', 'title'], 'required'],
            [['description'], 'string'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parentCategoryId' => 'Parent Category ID',
            'description' => 'Description',
            'title' => 'Title',
            'isActive' => 'Is Active',
        ];
    }
}
