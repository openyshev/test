<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property string $text
 * @property string $description
 * @property string $date
 * @property string $title
 * @property string $image
 * @property integer $isActive
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text', 'description', 'date', 'title', 'image'], 'required'],
            [['text', 'description'], 'string'],
            [['date'], 'safe'],
            [['isActive'], 'integer'],
            [['title', 'image'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Text',
            'description' => 'Description',
            'date' => 'Date',
            'title' => 'Title',
            'image' => 'Image',
            'isActive' => 'Is Active',
        ];
    }

    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['id' => 'categoryId'])->viaTable('newsCategory', ['newsId' => 'id']);
    }
}
