<?php
/* @var $this yii\web\View */
$this->title = 'Новостная страница';
?>
<div class="site-index" data-ng-controller="SiteController">
    <div class="content">
        <div class="row">
            <section data-ng-cloak class="col-xs-8" data-ng-init="getNewsList(true)">
                <h2>Список новостей</h2>
                <div data-ng-repeat="news in newsList | filter:filterByCategory">
                    <h3>{{news.title}}</h3>
                    <img data-ng-src="{{news.image}}" class="img-rounded">
                    <p>{{news.description}}</p>
                    <p>{{news.date}}</p>
                    <p><a href="/news/{{news.id}}">Перейти к новости >></a></p>
                </div>
            </section>
            <div data-ng-cloak class="col-xs-4" data-ng-init="getCategoryList()">
                <h2>Список категорий</h2>
                <ul>
                    <li data-ng-repeat="category in categoryList">
                        <a data-ng-click="changeSelectedCategory(category.id)">{{category.title}}</a><br>
                        <i>{{category.description}}</i>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>