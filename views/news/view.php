<div class="site-index" data-ng-controller="NewsController">
    <div class="content" data-ng-init="getNews('/news/', true)">
        <div data-ng-cloak class="row">
            <h1>{{news.title}}</h1>
            <p>{{news.text}}</p>
            <p>{{news.date}}</p>
        </div>
    </div>
</div>