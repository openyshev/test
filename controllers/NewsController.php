<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;

class NewsController extends Controller
{
    public function actionView()
    {
        if (!isset($_GET['id'])) {
            return $this->render('/site/index');
        }

        return $this->render('view');
    }
}