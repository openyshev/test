<?php

namespace app\commands;

use app\models\Category;
use app\models\News;
use app\models\NewsCategory;
use yii\base\ErrorException;
use yii\console\Controller;

class ParsingDataController extends Controller
{
    protected $newsCategory = [];

    public function actionIndex()
    {
        if (file_exists(\Yii::$app->basePath . '/data/data.json')) {
            $json = file_get_contents(\Yii::$app->basePath . '/data/data.json');
            $data = json_decode($json, true);

            echo 'Parsing Data was start..' . PHP_EOL;
            echo '-------------------------------------' . PHP_EOL;
            $this->parsingData($data);
            echo '-------------------------------------' . PHP_EOL;
            echo 'Parsing Data was successful complete!' . PHP_EOL;
        } else {
            throw new ErrorException('data.json file is not exists..');
        }
    }

    protected function parsingData($data)
    {
        foreach ($data as $categoryData) {
            $this->parsingCategory($categoryData);
        }

        foreach ($this->newsCategory as $newsId => $categoryId) {
            $news = News::findOne($newsId);
            $category = Category::findOne($categoryId);
            $newsCategory = NewsCategory::findOne(['newsId' => $newsId, 'categoryId' => $categoryId]);

            if (!$newsCategory && $news && $category) {
                $newsCategory = new NewsCategory();
                $newsCategory->newsId = (int)$newsId;
                $newsCategory->categoryId = (int)$categoryId;

                if (!$newsCategory->save()) {
                    echo print_r($newsCategory->getErrors(), true);
                    throw new ErrorException('Relation Category #' . $categoryId . ' and News #' . $newsId . ' fail saving..');
                } else {
                    echo 'Relation Category #' . $categoryId . ' and News #' . $newsId . ' successful saving' . PHP_EOL;
                }
            }
        }
    }

    protected function parsingCategory($categoryData, $parentCategoryId = null)
    {
        $subcategories = $categoryData['subcategories'];
        $news = $categoryData['news'];

        unset($categoryData['subcategories']);
        unset($categoryData['news']);

        if ($categoryData['isActive']) {
            if (!$category = Category::findOne($categoryData['id'])) {
                $category = new Category();
            }

            if ($parentCategoryId && !Category::findOne($parentCategoryId)) {
                $parentCategoryId = null;
            }

            $categoryData['parentCategoryId'] = $parentCategoryId;
            $categoryData['isActive'] = (int)$categoryData['isActive'];
            $category->setAttributes($categoryData);
            $category->id = $categoryData['id'];

            if (!$category->save()) {
                echo print_r($category->getErrors(), true);
                throw new ErrorException('Category #' . $category->id . ' fail saving.. CategoryData : ' . print_r($categoryData, true));
            } else {
                echo 'Category #' . $category->id . ' successful saving' . PHP_EOL;
            }
        }

        foreach ($subcategories as $subcategoryData) {
            $this->parsingCategory($subcategoryData, $categoryData['id']);
        }

        foreach ($news as $newsData) {
            $this->parsingNews($newsData, $categoryData['id']);
            $this->newsCategory[$newsData['id']] = $categoryData['id'];
        }
    }

    protected function parsingNews($newsData)
    {
        if ($newsData['isActive']) {
            if (!$news = News::findOne($newsData['id'])) {
                $news = new News();
            }

            $newsData['isActive'] = (int)$newsData['isActive'];
            $news->setAttributes($newsData);
            $news->id = $newsData['id'];

            if (!$news->save()) {
                echo print_r($news->getErrors(), true);
                throw new ErrorException('News #' . $news->id . ' fail saving.. NewsData : ' . print_r($newsData, true));
            } else {
                echo 'News #' . $news->id . ' successful saving' . PHP_EOL;
            }
        }
    }
}


